package preo.test.net.testpreo.ObjectsModels;

import android.content.Context;

import java.io.Serializable;

import preo.test.net.testpreo.R;

/**
 * Created by jconsuegra on 21/04/18.
 */

public class RatesResult  {

    public static  enum TYPERATE {GBP,EUR,JPY,BRL,USD};
    private String nameRate;
    private float value;
    private TYPERATE typerate;


    public  RatesResult(){

    }

    public TYPERATE getTyperate() {
        return typerate;
    }

    public void setTyperate(TYPERATE typerate) {
        this.typerate = typerate;
    }




    public String getNameRate(Context context) {

        switch (typerate)
        {
            case USD:
                nameRate = context.getString(R.string.USD);
                break;
            case BRL:
                nameRate = context.getString(R.string.BRL);
                break;
            case EUR:
                nameRate = context.getString(R.string.EUR);
                break;
            case GBP:
                nameRate = context.getString(R.string.GBP);
                break;
            case JPY:
                nameRate = context.getString(R.string.JPY);
                break;
            default:
                nameRate = context.getString(R.string.USD);
                break;
        }

        return nameRate;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }




}
