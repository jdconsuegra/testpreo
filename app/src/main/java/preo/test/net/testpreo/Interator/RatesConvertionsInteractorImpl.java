package preo.test.net.testpreo.Interator;

import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import preo.test.net.testpreo.Connection.APIClient;
import preo.test.net.testpreo.Connection.APIInterface;
import preo.test.net.testpreo.ObjectsModels.RatesConvertionsClass;
import preo.test.net.testpreo.Presenter.RatesConvertionsPresenterImp;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jconsuegra on 21/04/18.
 */

public class RatesConvertionsInteractorImpl implements RatesConvertionsInteractor {
    APIInterface apiInterface;

    public RatesConvertionsInteractorImpl() {
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @Override
    public void getRates(final OnFinishedListener onFinishedListener) {

        Call<RatesConvertionsClass> call = apiInterface.getRatesConvertionsOfUSD();
        call.enqueue(new Callback<RatesConvertionsClass>() {
            @Override
            public void onResponse(Call<RatesConvertionsClass> call, Response<RatesConvertionsClass> response) {
                onFinishedListener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<RatesConvertionsClass> call, Throwable t) {
                call.cancel();
                onFinishedListener.onError();
            }
        });


    }
}
