package preo.test.net.testpreo.Presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import preo.test.net.testpreo.Interator.RatesConvertionsInteractor;
import preo.test.net.testpreo.ObjectsModels.RatesConvertionsClass;
import preo.test.net.testpreo.ObjectsModels.RatesResult;
import preo.test.net.testpreo.R;

/**
 * Created by jconsuegra on 21/04/18.
 */



public class RatesConvertionsPresenterImp implements RatesConvertionsPresenter, RatesConvertionsInteractor.OnFinishedListener {

    private MainView mainView;
    private RatesConvertionsInteractor ratesConvertionsInterator;
    private Context context;
    RatesConvertionsClass ratesConvertionsClass;

    public RatesConvertionsPresenterImp(MainView mainView, RatesConvertionsInteractor ratesConvertionsInterator, Context context) {

        this.mainView = mainView;
        this.ratesConvertionsInterator = ratesConvertionsInterator;
        this.context = context;

    }

    @Override
    public void getRates() {

        Gson gson = new Gson();
        ratesConvertionsClass = gson.fromJson(getBackupRates(), RatesConvertionsClass.class);
        boolean flagCall = true;
        if (ratesConvertionsClass != null && ratesConvertionsClass.date != null  ) {
            Date currentDate = removeTime(new Date(System.currentTimeMillis()));
            if (!ratesConvertionsClass.date.before(currentDate)) {
                flagCall = false;
            }
        }
        if (flagCall) {
            if (mainView != null) {
                mainView.showProgress();
            }
            ratesConvertionsInterator.getRates(this);
        }
        else{
            mainView.updateLastConnection(DateFormat.getDateInstance().format(ratesConvertionsClass.date));
        }


    }

    @Override
    public void convertRates(int value) {
        Gson gson = new Gson();
        List<RatesResult> ratesResultList = new ArrayList<RatesResult>() {};
        if (ratesConvertionsClass!=null && ratesConvertionsClass.rates!=null){
            for (RatesResult.TYPERATE typerate:
                    RatesResult.TYPERATE.values()) {
                try {
                    RatesResult ratesResult = new RatesResult();
                    ratesResult.setTyperate(typerate);
                    ratesResult.setValue(value*ratesConvertionsClass.rates.get(typerate.name()));
                    ratesResultList.add(ratesResult);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
            Type listType = new TypeToken<List<RatesResult>>() {}.getType();
            String str=gson.toJson(ratesResultList,listType);
            mainView.showRatesConvert(str);
        }
        else{

            mainView.showError(context.getString(R.string.No_Data_Error));
        }

    }

    @Override
    public void onError() {
        mainView.hideProgress();
        mainView.showError(context.getString(R.string.Connection_error));
        if (ratesConvertionsClass==null || ratesConvertionsClass.date==null) {
            mainView.updateLastConnection(context.getString(R.string.never));
        }
        else{
            mainView.updateLastConnection(DateFormat.getDateInstance().format(ratesConvertionsClass.date));
        }


    }

    @Override
    public void onSuccess(RatesConvertionsClass ratesConvertionsClass) {
        mainView.hideProgress();
        Gson gson = new Gson();
        String ratesJson = gson.toJson(ratesConvertionsClass);
        setBackupRates(ratesJson);
        this.ratesConvertionsClass = ratesConvertionsClass;
        mainView.updateLastConnection(DateFormat.getDateInstance().format(ratesConvertionsClass.date));
    }


    private void setBackupRates(String rates) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.rates_key), rates);
        Date currentDate = removeTime(new Date(System.currentTimeMillis()));
        editor.putString(context.getString(R.string.last_connection), DateFormat.getDateInstance().format(currentDate));
        editor.commit();
        editor.apply();
    }

    private String getBackupRates() {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref.getString(context.getString(R.string.rates_key), null);
    }
    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
