package preo.test.net.testpreo.Components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import preo.test.net.testpreo.ObjectsModels.RatesResult;
import preo.test.net.testpreo.R;

/**
 * Created by jconsuegra on 21/04/18.
 */

public class RatesAdapter extends ArrayAdapter<RatesResult> {
    private LayoutInflater layoutInflater;


    public RatesAdapter(Context context, List<RatesResult> objects) {
        super(context, 0, objects);
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // holder pattern
        Holder holder = null;
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.listview_row, null);
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.textViewTitle));
            holder.setTxtDescription((TextView) convertView.findViewById(R.id.txtDescription));
            holder.setTxtType((TextView) convertView.findViewById(R.id.txttype));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final RatesResult row = getItem(position);
        holder.getTextViewTitle().setText(String.valueOf(row.getValue()));
        holder.getTxtType().setText(row.getTyperate().toString());
        holder.getTxtDescription().setText(row.getNameRate(getContext()));
        return convertView;
    }
    class Holder {
        TextView txtType;
        TextView textViewTitle;
        TextView txtDescription;
        View viewSeparator;

        public TextView getTextViewTitle() {
            return textViewTitle;
        }

        public void setTextViewTitle(TextView textViewTitle) {
            this.textViewTitle = textViewTitle;
        }

        public TextView getTxtDescription() {
            return txtDescription;
        }

        public void setTxtDescription(TextView textViewSubtitle) {
            this.txtDescription = textViewSubtitle;
        }

        public TextView getTxtType() {
            return txtType;
        }

        public void setTxtType(TextView txtType) {
            this.txtType = txtType;
        }




        public View getViewSeparator() {
            return viewSeparator;
        }

        public void setViewSeparator(View viewSeparator) {
            this.viewSeparator = viewSeparator;
        }



    }
}
