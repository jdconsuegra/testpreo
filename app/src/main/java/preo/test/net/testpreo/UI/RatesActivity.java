package preo.test.net.testpreo.UI;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import preo.test.net.testpreo.Components.RatesAdapter;
import preo.test.net.testpreo.ObjectsModels.RatesResult;
import preo.test.net.testpreo.R;

public class RatesActivity extends AppCompatActivity {

    private ArrayList<RatesResult>  ratesResultsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rates);
        Intent i = getIntent();
        Gson gson = new Gson();
        ratesResultsList = gson.fromJson(i.getStringExtra(this.getString(R.string.rates_convert)), new TypeToken<ArrayList<RatesResult>>() {
        }.getType());
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeButtonEnabled(true);

        ListView listfav = (ListView) findViewById(R.id.list);
        RatesAdapter adapter = new RatesAdapter(this, ratesResultsList);
        listfav.setAdapter(adapter);
        Button backButton = (Button)findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }
}
