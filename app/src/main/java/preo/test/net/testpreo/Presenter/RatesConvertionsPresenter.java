package preo.test.net.testpreo.Presenter;

/**
 * Created by jconsuegra on 21/04/18.
 */

public interface RatesConvertionsPresenter {

     void getRates();
     void convertRates(int value);
}
