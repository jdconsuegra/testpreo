package preo.test.net.testpreo.Interator;

import preo.test.net.testpreo.ObjectsModels.RatesConvertionsClass;

/**
 * Created by jconsuegra on 21/04/18.
 */

public interface RatesConvertionsInteractor {

    interface OnFinishedListener {

        void onError();

        void onSuccess(RatesConvertionsClass ratesConvertionsClass);
    }

    void getRates(final OnFinishedListener onFinishedListener);
}
