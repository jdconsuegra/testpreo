package preo.test.net.testpreo.Connection;

import preo.test.net.testpreo.ObjectsModels.RatesConvertionsClass;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by jconsuegra on 21/04/18.
 */

public interface APIInterface {

    @GET("/latest?base=USD")
    Call<RatesConvertionsClass> getRatesConvertionsOfUSD();
}
