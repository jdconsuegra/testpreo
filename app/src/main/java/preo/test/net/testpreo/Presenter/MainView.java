package preo.test.net.testpreo.Presenter;

import java.util.List;

import preo.test.net.testpreo.ObjectsModels.RatesResult;

/**
 * Created by jconsuegra on 21/04/18.
 */

public interface MainView {

    void showProgress();

    void hideProgress();

    void showError(String error);

    void updateLastConnection(String date);

    void showRatesConvert(String ratesResults);
}
