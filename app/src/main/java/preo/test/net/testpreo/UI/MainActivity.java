package preo.test.net.testpreo.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import preo.test.net.testpreo.Interator.RatesConvertionsInteractorImpl;
import preo.test.net.testpreo.ObjectsModels.RatesResult;
import preo.test.net.testpreo.Presenter.MainView;
import preo.test.net.testpreo.Presenter.RatesConvertionsPresenter;
import preo.test.net.testpreo.Presenter.RatesConvertionsPresenterImp;
import preo.test.net.testpreo.R;

public class MainActivity extends AppCompatActivity  implements MainView {


    Button btnConvert;

    private RatesConvertionsPresenter presenter;
    private ProgressDialog dialog;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new RatesConvertionsPresenterImp(this, new RatesConvertionsInteractorImpl(),this);
        presenter.getRates();
        btnConvert  = (Button)findViewById(R.id.btnConvert);
        context = this;
        final EditText valueEditText  = (EditText)findViewById(R.id.valueEditText);
        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isIntegerValidate=false;
                if (!valueEditText.getText().toString().equals("")){
                    if (isInteger(valueEditText.getText().toString())){
                        isIntegerValidate=true;
                    }
                }
                if (isIntegerValidate){
                    presenter.convertRates(Integer.parseInt(valueEditText.getText().toString()));
                }
                else{
                    showError(context.getString(R.string.whrite_correctValue));
                }
            }
        });

        setLastConnection();


    }

    @Override
    public void showProgress() {
        dialog = ProgressDialog.show(this, this.getString(R.string.consulting_string), this.getString(R.string.please_wait));
    }

    @Override
    public void hideProgress() {
        dialog.cancel();

    }

    @Override
    public void showError(String error) {
        Toast.makeText(this,error,Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateLastConnection(String date) {
        TextView lastUpdate = (TextView)findViewById(R.id.txtUpdate);
        lastUpdate.setText(this.getString(R.string.last_update) + ": " + date);
        setLastConnection();
    }

    @Override
    public void showRatesConvert(String ratesResults) {
        Intent intent = new Intent(this,RatesActivity.class);
        intent.putExtra(this.getString(R.string.rates_convert), ratesResults);
        startActivity(intent);
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    private void setLastConnection() {
        SharedPreferences sharedPref = this.getSharedPreferences(this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        TextView lastconnection =(TextView) findViewById(R.id.txtLastConnetion);
        lastconnection.setText(this.getString(R.string.last_update_server) + ": " + sharedPref.getString(this.getString(R.string.last_connection),this.getString(R.string.never) ));
    }
}
