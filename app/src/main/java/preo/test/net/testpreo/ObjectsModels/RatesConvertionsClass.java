package preo.test.net.testpreo.ObjectsModels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jconsuegra on 21/04/18.
 */

public class RatesConvertionsClass {


    @SerializedName("base")
    public String base;
    @SerializedName("date")
    public Date date;
    @SerializedName("rates")
    public Map<String,Float> rates = new HashMap<String, Float>() {} ;


}
